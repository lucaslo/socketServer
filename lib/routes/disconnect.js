import {usersRepository} from "../Users"
import {onlineUsers} from "../util/onlineUsers"

function disconnect (userToDisconnect){
  if (this.socket.user){
    console.log(this.socket.user.nickname + " just disconnected")
    this.socket.user.status = "offline"
    let users = onlineUsers()
    let action = {type: "DISCONNECT_USER", from: "System", text: this.socket.user.nickname + " just Disconnected", users: users}
    this.io.to("/").emit("systemMessage", action);
  }

}

export default disconnect
