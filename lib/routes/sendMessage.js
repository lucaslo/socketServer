import {usersRepository} from "../Users"

function sendMessage (message){
  console.log(this.socket.user.nickname + " just sended a message")

  let action = {type: "NEW_MESSAGE", from: this.socket.user.nickname, text: message}
  this.io.to("/").emit("systemMessage", action);

}

export default sendMessage
