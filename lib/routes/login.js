import {onlineUsers} from "../util/onlineUsers"

function login (userTologin){
  this.socket.user.nickname = userTologin
  this.socket.user.status = "online"

  console.log(this.socket.user.nickname + " just logged")
  let action = {type:"LOGIN_SUCCESS", id: this.socket.id, nickname: userTologin}
  this.socket.emit("systemMessage", action);
  let users = onlineUsers()
  action = {type: "CONNECT_USER", from: "System", text: this.socket.user.nickname + " just connected", users: users}
  this.io.to("/").emit("systemMessage", action);

}

export default login
