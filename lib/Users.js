export const usersRepository = []

export class User {
  constructor(socket, nickname){
    this.nickname = nickname
    this.socketId = socket.id
    this.socket = socket
    this.status = "online"
  }
}
