import {User, usersRepository} from "./Users"
import disconnect from "./routes/disconnect"
import login from "./routes/login"
import sendMessage from "./routes/sendMessage"


const server = require('http').createServer();
const io = require('socket.io')(server, {
  serveClient: false,
  wsEngine: 'ws' // uws is not supported since it is a native module
});
const port = process.env.PORT || 3003;

io.on('connect', onConnect);
server.listen(port, () => console.log('server listening on port ' + port));

function onConnect(socket){
  let user = new User(socket, "teste")
  usersRepository.push(user)
  socket.user = user
  socket.join("/")
  socket.on('disconnect',disconnect.bind({socket, io}))
  socket.on('login',login.bind({socket, io}))
  socket.on('message',sendMessage.bind({socket, io}))

}
