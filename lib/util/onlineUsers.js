import {usersRepository} from "../Users"

export function onlineUsers(){
  let users = usersRepository.filter(user =>{
    return user.status!== "offline"
  })
  users = users.map((user) =>{
    var userToSend = user
    delete userToSend.socket

    return userToSend
  })

  return users
}
