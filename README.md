
# Webpack socketServer

Um servidor de chat utilizando socket.io e Webserver!
A interface do cliente encontra-se em https://gitlab.com/lucaslo/chat

## Como Utilizar

Primeiro clone esse repositório e navegue para o diretório utilizando uma interface de linha de comando.
É nescessário ter node.js e npm instalados!


```bash
$ npm install
```
```bash
$ npm  run build
$ npm  run start
```
