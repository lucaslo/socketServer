
module.exports = {
  entry: './lib/index.js',
  target: 'node',
  output: {
    path: require('path').join(__dirname, '../dist'),
    filename: 'server.js'
  },
  devtool: 'inline',
module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }
  ]
}
};
